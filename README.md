[![Latest release](https://gitlab.com/smartefact/smartefact.java.xdg.basedir/-/badges/release.svg?key_text=Latest%20release&key_width=100)](https://gitlab.com/smartefact/smartefact.java.xdg.basedir/-/releases)
[![Pipeline status](https://gitlab.com/smartefact/smartefact.java.xdg.basedir/badges/main/pipeline.svg?key_text=Pipeline%20status&key_width=100)](https://gitlab.com/smartefact/smartefact.java.xdg.basedir/-/commits/main)
[![Coverage](https://gitlab.com/smartefact/smartefact.java.xdg.basedir/badges/main/coverage.svg?key_text=Coverage&key_width=100)](https://gitlab.com/smartefact/smartefact.java.xdg.basedir/-/commits/main)

# smartefact.java.xdg.basedir

Java utilities for the [XDG Base Directory specification](https://www.freedesktop.org/wiki/Specifications/basedir-spec/).

[Javadoc](https://smartefact.gitlab.io/smartefact.java.xdg.basedir)

## Changelog

See [CHANGELOG](CHANGELOG.md).

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md).

## License

This project is licensed under the [Apache License 2.0](LICENSE).
