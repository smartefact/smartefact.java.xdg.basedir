/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.xdg.basedir;

import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assumptions.abort;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

@SuppressWarnings("NullAway")
class XdgBaseDirTest {
    static XdgBaseDir xdgBaseDir;

    @BeforeAll
    static void initXdgBaseDir() {
        try {
            xdgBaseDir = XdgBaseDir.getInstance();
        } catch (IllegalStateException e) {
            abort("The platform is not supported");
        }
    }

    @Test
    void getCandidateConfigDirs() {
        final List<Path> candidateConfigDirs = xdgBaseDir.getCandidateConfigDirs().collect(Collectors.toList());
        assertFalse(candidateConfigDirs.isEmpty());
    }

    @Test
    void getCandidateDataDirs() {
        final List<Path> candidateDataDirs = xdgBaseDir.getCandidateDataDirs().collect(Collectors.toList());
        assertFalse(candidateDataDirs.isEmpty());
    }
}
