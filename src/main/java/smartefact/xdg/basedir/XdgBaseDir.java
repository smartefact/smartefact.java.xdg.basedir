/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.xdg.basedir;

import java.nio.file.Path;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import smartefact.Collections;
import static smartefact.Preconditions.checkNotNull;
import smartefact.Strings;
import smartefact.SystemProperties;
import static smartefact.ToStringBuilder.toStringBuilder;

/**
 * Utilities for the <em>XDG Base Directory specification</em>.
 * <p>
 * <b>Specification:</b> XDG Base Directory 0.8
 * </p>
 *
 * @author Laurent Pireyn
 * @see <a href="https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html">XDG Base Directory Specification</a>
 */
public final class XdgBaseDir {
    private static final String HOME = "HOME";
    private static final String XDG_CACHE_HOME = "XDG_CACHE_HOME";
    private static final String XDG_CONFIG_DIRS = "XDG_CONFIG_DIRS";
    private static final String XDG_CONFIG_HOME = "XDG_CONFIG_HOME";
    private static final String XDG_DATA_DIRS = "XDG_DATA_DIRS";
    private static final String XDG_DATA_HOME = "XDG_DATA_HOME";
    private static final String XDG_RUNTIME_DIR = "XDG_RUNTIME_DIR";
    private static final String XDG_STATE_HOME = "XDG_STATE_HOME";
    private static final char PATH_SEPARATOR = ':';

    private static @Nullable XdgBaseDir instance;

    public static @NotNull XdgBaseDir getInstance() {
        synchronized (XdgBaseDir.class) {
            if (instance == null) {
                final @NotNull String fileSeparator = SystemProperties.getFileSeparator();
                if (!fileSeparator.equals("/")) {
                    throw new IllegalStateException(
                        "The current platform does not support the XDG Base Directory specification, " +
                            "as it uses <" + fileSeparator + "> as file separator"
                    );
                }
                instance = new XdgBaseDir();
            }
            return instance;
        }
    }

    @Contract(pure = true)
    private static @Nullable String getEnvVar(@NotNull String name) {
        final @Nullable String value;
        try {
            value = System.getenv(name);
        } catch (SecurityException e) {
            return null;
        } catch (NullPointerException e) {
            throw new AssertionError(e);
        }
        return !Strings.isNullOrBlank(value) ? value : null;
    }

    @Contract(pure = true)
    private static @NotNull String getRequiredEnvVar(@NotNull String name) {
        return checkNotNull(
            getEnvVar(name),
            () -> "The environment variable <" + name + "> is undefined"
        );
    }

    @Contract(pure = true)
    private static @Nullable Path getEnvVarAsPath(@NotNull String name) {
        final @Nullable String value = getEnvVar(name);
        return value != null ? Path.of(value) : null;
    }

    @Contract(pure = true)
    private static @NotNull Path getEnvVarAsPath(
        @NotNull String name,
        @NotNull Supplier<? extends @NotNull Path> defaultValue
    ) {
        final @Nullable Path value = getEnvVarAsPath(name);
        return value != null ? value : defaultValue.get();
    }

    @Contract(pure = true)
    private static @NotNull List<@NotNull Path> getEnvVarAsPathList(
        @NotNull String name,
        @NotNull Supplier<? extends @NotNull List<@NotNull Path>> defaultValue
    ) {
        final @Nullable String value = getEnvVar(name);
        return value != null ? toPathList(value) : defaultValue.get();
    }

    @Contract(pure = true)
    private static @NotNull List<@NotNull Path> toPathList(@NotNull CharSequence string) {
        return Collections.map(
            Strings.split(string, PATH_SEPARATOR),
            Path::of
        );
    }

    @Contract(pure = true)
    private static @NotNull Stream<@NotNull Path> getExistingFiles(
        @NotNull Stream<? extends @NotNull Path> candidateDirs,
        @NotNull String dirName,
        @NotNull String fileName
    ) {
        final @NotNull Path filePath = Path.of(dirName, fileName);
        return candidateDirs
            .map(dir -> dir.resolve(filePath))
            .filter(file -> file.toFile().exists());
    }

    private final @NotNull Path home = Path.of(getRequiredEnvVar(HOME));

    private final @NotNull Path cacheHome = getEnvVarAsPath(
        XDG_CACHE_HOME,
        () -> home.resolve(".cache")
    );

    private final @NotNull Path configHome = getEnvVarAsPath(
        XDG_CONFIG_HOME,
        () -> home.resolve(".config")
    );

    private final @NotNull Path dataHome = getEnvVarAsPath(
        XDG_DATA_HOME,
        () -> home.resolve(".local/share")
    );

    private final @NotNull Path stateHome = getEnvVarAsPath(
        XDG_STATE_HOME,
        () -> home.resolve(".local/state")
    );

    private final @Nullable Path runtimeDir = getEnvVarAsPath(XDG_RUNTIME_DIR);

    private final @NotNull Path binDir = home.resolve(".local/bin");

    private final @NotNull List<@NotNull Path> configDirs = getEnvVarAsPathList(
        XDG_CONFIG_DIRS,
        () -> List.of(Path.of("/etc/xdg"))
    );

    private final @NotNull List<@NotNull Path> dataDirs = getEnvVarAsPathList(
        XDG_DATA_DIRS,
        () -> List.of(Path.of("/usr/local/share"), Path.of("/usr/share"))
    );

    private XdgBaseDir() {}

    /**
     * Returns the home directory.
     * <p>
     * This implementation is based on the {@code HOME} environment variable,
     * not on the {@code user.home} system property
     * (although they should have the same value).
     * </p>
     *
     * @return the home directory (never {@code null})
     */
    @Contract(pure = true)
    public @NotNull Path getHome() {
        return home;
    }

    /**
     * Returns the cached data directory.
     *
     * @return the cached data directory (never {@code null})
     */
    @Contract(pure = true)
    public @NotNull Path getCacheHome() {
        return cacheHome;
    }

    /**
     * Returns the configuration directory.
     *
     * @return the configuration directory (never {@code null})
     */
    @Contract(pure = true)
    public @NotNull Path getConfigHome() {
        return configHome;
    }

    /**
     * Returns the data directory.
     *
     * @return the data directory (never {@code null})
     */
    @Contract(pure = true)
    public @NotNull Path getDataHome() {
        return dataHome;
    }

    /**
     * Returns the state data directory.
     *
     * @return the state data directory (never {@code null})
     */
    @Contract(pure = true)
    public @NotNull Path getStateHome() {
        return stateHome;
    }

    /**
     * Returns the runtime directory.
     * <p>
     * This may be {@code null} as it has no default value.
     * </p>
     *
     * @return the runtime directory (maybe {@code null})
     */
    @Contract(pure = true)
    public @Nullable Path getRuntimeDir() {
        return runtimeDir;
    }

    /**
     * Returns the user binary directory.
     *
     * @return the user binary directory (never {@code null})
     */
    @Contract(pure = true)
    public @NotNull Path getBinDir() {
        return binDir;
    }

    /**
     * Returns the configuration directories.
     *
     * @return the configuration directories (never {@code null})
     */
    @Contract(pure = true)
    public @NotNull List<@NotNull Path> getConfigDirs() {
        return configDirs;
    }

    /**
     * Returns the data directories.
     *
     * @return the data directories (never {@code null})
     */
    @Contract(pure = true)
    public @NotNull List<@NotNull Path> getDataDirs() {
        return dataDirs;
    }

    @Contract(pure = true)
    public @NotNull Stream<@NotNull Path> getCandidateConfigDirs() {
        return Stream.concat(
            Stream.of(configHome),
            configDirs.stream()
        );
    }

    @Contract(pure = true)
    public @NotNull Stream<@NotNull Path> getCandidateDataDirs() {
        return Stream.concat(
            Stream.of(dataHome),
            dataDirs.stream()
        );
    }

    @Contract(pure = true)
    public @NotNull Stream<@NotNull Path> getConfigFiles(
        @NotNull String dirName,
        @NotNull String fileName
    ) {
        return getExistingFiles(
            getCandidateConfigDirs(),
            dirName,
            fileName
        );
    }

    @Contract(pure = true)
    public @NotNull Stream<@NotNull Path> getDataFiles(
        @NotNull String dirName,
        @NotNull String fileName
    ) {
        return getExistingFiles(
            getCandidateDataDirs(),
            dirName,
            fileName
        );
    }

    @Override
    @Contract(pure = true)
    public String toString() {
        return toStringBuilder(this)
            .property("home", home)
            .property("cacheHome", cacheHome)
            .property("configHome", configHome)
            .property("dataHome", dataHome)
            .property("stateHome", stateHome)
            .property("runtimeDir", runtimeDir)
            .property("binDir", binDir)
            .property("configDirs", configDirs)
            .property("dataDirs", dataDirs)
            .build();
    }
}
